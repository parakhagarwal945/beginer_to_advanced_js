// The JavaScript this keyword refers to the object it belongs to.

// It has different values depending on where it is used:

// In a method, this refers to the owner object.
// Alone, this refers to the global object.
// In a function, this refers to the global object.
// In a function, in strict mode, this is undefined.
// In an event, this refers to the element that received the event.
// Methods like call(), and apply() can refer this to any object.


//example 1
var person = {
    firstName: "John",
    lastName : "Doe",
    id     : 5566,
    fullName : function() {
      return this.firstName + " " + this.lastName;
    }
  };
  console.log(person.fullName());
  console.log(typeof(person.fullName));
  console.log(typeof(person.fullName()));
  //here this refers as owner object


//   this Alone
//   When used alone, the owner is the Global object, so this refers to the Global object.
  
//   In a browser window the Global object is [object Window]:
  var x=this;
  console.log(x);
  console.log(typeof(x))


  //this in function
  function sum(a,b){
    // console.log(a+b)
  return this;
  }
  sum(3,7)



 