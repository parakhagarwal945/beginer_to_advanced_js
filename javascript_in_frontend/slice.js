// The slice() method copies a portion of an array to a new array. Suppose we want to copy the last two items in the array to a new array. We would start with the index number of the first element we want, which is 2 for koi. We would end with the index number one more than the index of the last element we want. Because the last element, eel, has the index number of 3, we would put 4.



var fish = ['piranha','seahorse','koi','eel'];
var fish1 = fish.slice(2,4);
console.log("New fish",fish1);

var fruits = ["Banana", "Orange", "Lemon", "Apple", "Mango"];
var citrus = fruits.slice(1);
console.log("Sliced fruits array ", citrus);

var num = [11,22,33,44,55,66];
var num1 = num.slice(1,4);
console.log("Sliced num array ", num1);


var array=[1,2,3,4,5];
console.log("Result is",array.slice(2));
console.log("Result is ",array.slice(-2));
console.log('Original array ',array);


var array2=[6,7,8,9,0];
console.log('Result is ',array2.slice(2,4));
console.log('Result is ',array2.slice(-2,4));
console.log('Result is ',array2.slice(-3,-1));
console.log('Original array ',array2);


var fruits=["kiwi","orange","apple","mango","banana","guava"]
console.log(fruits.slice(-3,2))



// advantage of using slice is our original array remains unchanged


  //extract bananas from the below string 
  //method 1
//   in this i put the string in an array and then using loop i exract the value
var txt = "I can eat bananas all day";
var txt1=(txt.split(" "));
var txt2=(txt1.slice(3,4));
for(var i=0;i<1;i++){
    console.log(txt2[i])
}


//method 2

var txt = "I can eat bananas all day";
console.log(txt.slice(10,17));