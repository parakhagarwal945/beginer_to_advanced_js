// push pop shift unshift
// push add in last
// pop remove from last
// shift  remove from first
// unshift add in first postion
// delete it creates holes
// sort alphabetically
// sort alphabetically and reverse
// sort numerically in ascending
// sort numerically in descending



// function push(array){
// console.log("before push array length is",array.length)
// let b=array.push(90);
// console.log("new array is",array)
// console.log("after push array length is",array.length)
// }

// push([11,22,33,44,55])


// function pop(array){
//     console.log("before pop array length is",array.length)
//     let b=array.pop();
//     console.log("new array is",array)
//     console.log("after pop array length is",array.length)
//     }
    
//     pop([11,22,33,44,55])



// function shift(array){
//     console.log("before shift array length is",array.length)
//     let b=array.shift();
//     console.log("new array is",array)
//     console.log("after shift array length is",array.length)
//     }
    
//     shift([11,22,33,44,55])




    // function unshift(array){
    //     console.log("before unshift array length is",array.length)
    //     let b=array.unshift(10);
    //     console.log("new array is",array)
    //     console.log("after unshift array length is",array.length)
    //     }
        
    //     unshift([11,22,33,44,55])



    // function delte(array){
    //     console.log("before delete array length is",array.length)
    //     delete array[3];
    //     console.log("new array is",array)
    //     console.log("after delete array length is",array.length)
    //     console.log("deleted element value is",array[3])
    //     console.log("type of created hole is",typeof(array[3]))
    //     }
        
    //     delte([11,22,33,44,55])

        // delete is a keyword in js so u can't use it as a function name and delete property will not reduce length of array



        // function sort_alphabetically(array){
        //    let b=array.sort();
        //    console.log("after sorting alphabetically array is",b)
        // }
        // sort_alphabetically(["Banana", "Orange", "Apple", "Mango"])



        // function sort_alphabetically_reverse(array){
        //     let b=array.sort().reverse();
        //     console.log("after sorting alphabetically and reverse it",b)
        //  }
        //  sort_alphabetically_reverse(["Banana", "Orange", "Apple", "Mango"])


        // function sort_numerically_ascending_order(array){
        //    let b=array.sort((a,b)=>{
        //        return a-b;
        //    });
        //    console.log("after sorting numerically array is",b)
        //  }
        //  sort_numerically_ascending_order( [40, 100, 1, 5, 25, 10])


        function sort_numerically_descending_order(array){
            let b=array.sort((a,b)=>{
                return b-a;
            });
            console.log(b)
          }
          sort_numerically_descending_order( [40, 100, 1, 5, 25, 10])




        //   If the result is negative a is sorted before b.

        //   If the result is positive b is sorted before a.
          
        //   If the result is 0 no changes are done with the sort order of the two values.




        