//what is the typeof whitespace
// ans is it gives the syntaxerror of console.log(typeof());
//string 
var a="parakh";
var b="12";
var c="";
var d=" ";
console.log(typeof(a));
console.log(typeof(b));
console.log(typeof(c));
console.log(typeof(d));
 //boolean
 var q=true;
 console.log(q)
 var w=false;
 console.log(w)

//number
var e=12;
var f=12.09;
var g=(23);
var h=(34+90);
var qq=Infinity;
var qa=NaN;
console.log(typeof(e));
console.log(typeof(f));
console.log(typeof(g));
console.log(typeof(h));
console.log("typeof infinity is",typeof(qq))
console.log("typeof NAN is",typeof(qa))

//undefined
var i=undefined;
console.log(typeof(i))
var k;
console.log("k is",typeof(k));


//null
var j=null;
console.log(typeof(j))



//controversy
console.log(null === undefined);  //false
console.log(null == undefined);   //true



//complex data type
//object
var n=[1,2,3,5];
console.log(" type of n is",typeof(n));

var o={
    name:"parakh",
    age:19,
}
console.log("type of o is",typeof(o));

var p=null;
console.log("type of p is",typeof(p))


//function
function sum(a,b){
console.log(a+b);
}
console.log(typeof(sum));

console.log(typeof function myFunc(){});


// The typeof operator returns "object" for arrays because in JavaScript arrays are objects.



// datatypes
// primitive number string boolean undefined null
// complex   object function



// The return statement ends function execution and specifies a value to be returned to the function caller.