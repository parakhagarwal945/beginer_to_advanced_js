// The splice() method can either add or remove an item, or add and remove items at the same time from any position in an array. It takes three parameters — the index number to start at, the number of items to remove, and items to add (optional). For eg. Splice (0, 0, "new") would add the string "new" to the beginning of an array, and delete nothing.
// The splice method can also be used to add or remove items from an array. Splice is a function in which the first parameter defines the position(index number) where new elements will be added. 2nd parameter defines how many elements will be removed. Rest of the parameters defines the new elements to be added as shown in the example below:


var fruits = ["Banana ", "Orange ", "Apple ", "Mango "];
fruits.splice(2, 0, "Lemon ", "Kiwi ");
console.log("New array is ", fruits);
//explaination of above line
// 2nd no ki index p 0 elements ko delete kiye lemon kiwi ko add kr diya


var num = [1, 2, 3, 4, 5, 6];
var removed = num.splice(2, 2, 7, 8);


console.log("Removed array is ", removed);

console.log("New array is ", num);


// splice se original array change ho jaata h