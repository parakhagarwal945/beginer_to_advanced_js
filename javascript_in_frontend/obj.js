// function carname(){
//     var car={
//         name:['audi','volvo','BMW'],
//         colour:"black",
//         price:5000000,
//     } 
//     console.log(car.name)
//     for(var i=0;i<car.name.length;i++){
//         console.log(car.name[i])
//     }
// }
// carname();


//example 2
var person = {
    firstName: "John",
    lastName : "Doe",
    id     : 5566,
    fullName : function() {
      return this.firstName + " " + this.lastName;
    }
  };
  console.log(person.fullName());
  console.log(typeof(person.fullName));
  console.log(typeof(person.fullName()));
//   in this case it refers as owner object
//   In a method, this refers to the owner object.
//   In a function definition, this refers to the "owner" of the function.

// In the example above, this is the person object that "owns" the fullName function.

// In other words, this.firstName means the firstName property of this object.